package com.fungorn.tutorstudentchatapp.data.api

import com.fungorn.tutorstudentchatapp.data.AccountRepository
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class AccessTokenAuthenticator(
    private val accountRepository: AccountRepository
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        val accessToken = accountRepository.getAccessToken()

        if (!isRequestWithAccessToken(response) || accessToken == null) {
            return null
        }

        synchronized(this) {
            val newAccessToken = accountRepository.getAccessToken()!! // FIXME: 15.11.2020  

            // Access token is refreshed in another thread.
            if (accessToken != newAccessToken) {
                return newRequestWithAccessToken(response.request, newAccessToken)
            }

            // Need to refresh an access token
            val updatedAccessToken = accountRepository.refreshToken()

            return newRequestWithAccessToken(response.request, updatedAccessToken)
        }
    }

    private fun isRequestWithAccessToken(response: Response): Boolean {
        val header = response.request.header("Authorization")

        return header != null && header.startsWith("Bearer")
    }

    private fun newRequestWithAccessToken(
        request: Request,
        accessToken: String
    ): Request? = request.newBuilder()
        .header("Authorization", "Bearer $accessToken")
        .build()
}