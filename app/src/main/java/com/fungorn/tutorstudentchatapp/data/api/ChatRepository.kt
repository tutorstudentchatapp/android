package com.fungorn.tutorstudentchatapp.data.api

import com.fungorn.tutorstudentchatapp.data.api.websocket.WebsocketChatService
import kotlinx.coroutines.flow.Flow

class ChatRepository(
    private val websocketChatService: WebsocketChatService
) {
    fun sendText(text: String) = websocketChatService.sendText(text)

    fun observeText(): Flow<String> {
        return websocketChatService.observeText()
    }
}