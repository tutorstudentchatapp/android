package com.fungorn.tutorstudentchatapp.data

import android.accounts.Account
import android.accounts.AccountManager
import com.fungorn.tutorstudentchatapp.domain.User
import com.fungorn.tutorstudentchatapp.domain.UserType
import org.joda.time.DateTime
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent


@OptIn(KoinApiExtension::class)
class AccountRepository(
    private val accountManager: AccountManager,
) : KoinComponent {
//    private lateinit var authRepository: AuthRepository by inject()

    fun loginUser(accessToken: String, user: User) {
        val currentAccount = getOrCreateAccount()

        updateToken(currentAccount, accessToken)
        updateUserInfo(currentAccount, user)

        accountManager.setUserData(currentAccount, IS_USER_LOGGED_IN, true.toString())
    }

    fun signOut() {
        val account = getAccount()

        if (account != null) {
            accountManager.setUserData(account, IS_USER_LOGGED_IN, false.toString())
        }
    }

    fun refreshToken(): String {
        // TODO : API request to update token
        // authRepository...
        val refreshedToken = ""

        val currentAccount = getAccount() ?: throw IllegalStateException("No current account")

        updateToken(currentAccount, refreshedToken)

        return refreshedToken
    }

    private fun updateToken(currentAccount: Account, accessToken: String) {
        accountManager.setAuthToken(currentAccount, ACCESS_TOKEN, accessToken)
    }

    private fun updateUserInfo(currentAccount: Account, userInfo: User) {
        accountManager.setUserData(currentAccount, USER_ID, userInfo.id.toString())

        accountManager.setUserData(
            currentAccount,
            USER_FULL_NAME,
            "${userInfo.lastName} ${userInfo.firstName} ${userInfo.middleName}"
        )

        accountManager.setUserData(currentAccount, USER_TYPE, userInfo.type.name)

        if (userInfo is User.Student) {
            accountManager.setUserData(currentAccount, USER_GROUP_NAME, userInfo.groupName)
        }
    }

    fun getAccessToken(): String? {
        val currentAccount = getAccount() ?: throw IllegalStateException("No current account")

        return accountManager.peekAuthToken(currentAccount, ACCESS_TOKEN)
    }

    fun getUserInfo(): User {
        val currentAccount = getAccount() ?: throw IllegalStateException("No current account")

        val userId = accountManager.getUserData(currentAccount, USER_ID).toInt()

        val fullName = accountManager.getUserData(currentAccount, USER_FULL_NAME)

        val lastName = fullName.substringBefore(' ')

        val firstName = fullName.substring(
            fullName.indexOfFirst { it == ' ' } + 1,
            fullName.indexOfLast { it == ' ' }
        )

        val middleName = fullName.substringAfterLast(' ')

        return when (UserType.valueOf(accountManager.getUserData(currentAccount, USER_TYPE))) {
            UserType.TUTOR -> User.Tutor(
                userId,
                firstName,
                lastName,
                middleName,
                DateTime.now(),
                ""
            )
            UserType.STUDENT -> {
                val groupName = accountManager.getUserData(currentAccount, USER_GROUP_NAME)

                User.Student(userId, firstName, lastName, middleName, DateTime.now(), groupName)
            }
        }
    }

    fun isUserLoggedIn(): Boolean {
        val currentAccount = getAccount()

        return if (currentAccount == null) false
        else {
            var isUserLoggedIn =
                accountManager.getUserData(currentAccount, IS_USER_LOGGED_IN)?.toBoolean()

            if (isUserLoggedIn == null) {
                isUserLoggedIn = !getAccessToken().isNullOrEmpty()

                accountManager.setUserData(
                    currentAccount,
                    IS_USER_LOGGED_IN,
                    isUserLoggedIn.toString()
                )
            }
            isUserLoggedIn
        }
    }

    private fun getOrCreateAccount(): Account {
        var account = getAccount()

        if (account == null) {
            account = Account(ACCOUNT_NAME, ACCOUNT_TYPE)

            accountManager.addAccountExplicitly(account, ACCOUNT_PASSWORD, null)
        }

        return account
    }

    private fun getAccount(): Account? {
        val accounts = accountManager.getAccountsByType(ACCOUNT_TYPE)
        if (accounts.count() > 1)
            throw IllegalStateException("$ACCOUNT_NAME accounts > 1")

        return accounts.singleOrNull()
    }

    companion object {
        private const val ACCOUNT_NAME = "TutorStudentChat"
        private const val ACCOUNT_TYPE = "com.fungorn.tutorstudentchat"
        private const val ACCOUNT_PASSWORD = "Password"

        private const val ACCESS_TOKEN = "$ACCOUNT_TYPE.token.access"

        private const val USER_ID = "$ACCOUNT_TYPE.user.id"
        private const val USER_FULL_NAME = "$ACCOUNT_TYPE.user.fullname"
        private const val USER_GROUP_NAME = "$ACCOUNT_TYPE.user.group"
        private const val USER_TYPE = "$ACCOUNT_TYPE.user.type"
        private const val IS_USER_LOGGED_IN = "$ACCOUNT_TYPE.user.isLoggedIn"
    }
}