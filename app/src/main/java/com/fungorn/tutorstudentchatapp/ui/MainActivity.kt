package com.fungorn.tutorstudentchatapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.ui.utils.DialogProvider
import com.fungorn.tutorstudentchatapp.ui.utils.ext.setupWithContentScaling
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_main_drawer.*


class MainActivity : AppCompatActivity() {

    private val navController: NavController
        get() = (supportFragmentManager
            .findFragmentById(R.id.fragmentContainerView_main_navHost) as NavHostFragment)
            .navController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView_main.setupWithNavController(navController)

        drawerLayout_main.setupWithContentScaling(
            this,
            constraintLayout_main_container,
            SLIDING_CONTENT_SCALE_FACTOR
        )

        setupDrawerContent()
    }

    override fun onBackPressed() {
        if (drawerLayout_main.isOpen)
            drawerLayout_main.close()
        else
            handleBackPress()
//            super.onBackPressed()
    }

    // TODO : Redesign with Auth Flow
    private fun handleBackPress() {
        if (!navController.popBackStack())
            moveTaskToBack(true)
    }

    private fun setupDrawerContent() {
        with(include_drawer) {
            linearLayout_drawer_profile

            textView_drawer_groups.setOnClickListener {
                drawerLayout_main.closeDrawer(this, true)
            }
            textView_drawer_people.setOnClickListener {
                drawerLayout_main.closeDrawer(this, true)
            }
            textView_drawer_help.setOnClickListener {
                drawerLayout_main.closeDrawer(this, true)

                navController.navigate(R.id.fragment_help)
            }
            textView_drawer_preferences.setOnClickListener {
                drawerLayout_main.closeDrawer(this, true)

                navController.navigate(R.id.fragment_settings)
            }
            textView_drawer_logout.setOnClickListener {
                drawerLayout_main.closeDrawer(this, true)

                DialogProvider.confirmationDialog(
                    this@MainActivity,
                    getString(R.string.login_logout_confirmation),
                    onConfirm = {
                        // TODO : Remove
                        finish()
                    },
                    onDecline = {}
                ).show()
            }
        }
    }

    companion object {
        const val SLIDING_CONTENT_SCALE_FACTOR = 6f
    }
}