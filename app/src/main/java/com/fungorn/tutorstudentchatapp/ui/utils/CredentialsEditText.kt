package com.fungorn.tutorstudentchatapp.ui.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import com.google.android.material.R.attr
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


class CredentialsEditText(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int
) : TextInputEditText(context, attrs, defStyleAttr) {

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, attr.editTextStyle)

    constructor(
        context: Context
    ) : this(context, null, attr.editTextStyle)


    override fun setError(error: CharSequence?, icon: Drawable?) {
        setCompoundDrawables(null, null, icon, null)
    }

    override fun onCreateInputConnection(outAttrs: EditorInfo): InputConnection? {
        val ic = super.onCreateInputConnection(outAttrs)
        if (ic != null && outAttrs.hintText == null) {
            outAttrs.hintText = this.getHintFromLayout()
        }

        return ic
    }

    private fun getTextInputLayout(): TextInputLayout? {
        var parent = this.parent
        while (parent is View) {
            if (parent is TextInputLayout) {
                return parent
            }
            parent = parent.getParent()
        }

        return null
    }

    private fun getHintFromLayout(): CharSequence? {
        val layout = this.getTextInputLayout()
        return layout?.hint
    }
}