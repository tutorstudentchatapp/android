package com.fungorn.tutorstudentchatapp.ui.direct

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.ui.direct.model.DirectDialogModel

class DirectDialogAdapter(
    private val onDialogClick: (DirectDialogModel) -> Unit
) : RecyclerView.Adapter<DirectDialogViewHolder>() {
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)

    fun updateItems(items: List<DirectDialogModel>) {
        differ.submitList(items)
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DirectDialogViewHolder {
        return DirectDialogViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_direct_dialog, parent, false)
        )
    }

    override fun onBindViewHolder(holder: DirectDialogViewHolder, position: Int) {
        holder.bind(differ.currentList[position], onDialogClick)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<DirectDialogModel>() {
            override fun areItemsTheSame(
                oldItem: DirectDialogModel,
                newItem: DirectDialogModel
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: DirectDialogModel,
                newItem: DirectDialogModel
            ): Boolean = oldItem == newItem
        }
    }
}