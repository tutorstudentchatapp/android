package com.fungorn.tutorstudentchatapp.ui.direct

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fungorn.tutorstudentchatapp.ui.direct.model.DirectDialogModel

class DirectViewModel : ViewModel() {
    private val _dialogs = MutableLiveData<List<DirectDialogModel>>()
    val dialogs: LiveData<List<DirectDialogModel>> = _dialogs

    init {
        _dialogs.value = listOf(
            DirectDialogModel(1, "Ivan Ivanov", "Latest message text", true),
            DirectDialogModel(2, "Petr Sharikov", "Latest message text", false),
            DirectDialogModel(3, "Alexey Petrov", "Latest message text", false)
        )
    }
}