package com.fungorn.tutorstudentchatapp.ui.direct.model

data class DirectDialogModel(
    val id: Int,
    //val profile: ProfileHeaderModel, // TODO
    val senderName: String,
    val latestMessageText: String,
    val hasUnreadMessages: Boolean
)