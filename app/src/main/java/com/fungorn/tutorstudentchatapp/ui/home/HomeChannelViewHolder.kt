package com.fungorn.tutorstudentchatapp.ui.home

import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.fungorn.tutorstudentchatapp.ui.home.model.HomeChannelModel
import kotlinx.android.synthetic.main.item_channel.view.*
import kotlinx.android.synthetic.main.item_direct.view.*
import kotlinx.android.synthetic.main.item_header.view.*

abstract class HomeChannelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(
        model: HomeChannelModel,
        onItemClickListener: HomeChannelAdapter.OnItemClickListener
    )
}

class HomeHeaderViewHolder(itemView: View) : HomeChannelViewHolder(itemView) {
    override fun bind(
        model: HomeChannelModel,
        onItemClickListener: HomeChannelAdapter.OnItemClickListener
    ) = with(itemView) {
        textView_header.text = model.title
    }
}

class HomeChannelItemViewHolder(itemView: View) : HomeChannelViewHolder(itemView) {
    override fun bind(
        model: HomeChannelModel,
        onItemClickListener: HomeChannelAdapter.OnItemClickListener
    ) = with(itemView) {
        model as HomeChannelModel.ChannelItem

        textView_channel_title.text = model.title

        imageView_channel_unreadIndicator.isVisible = model.hasUnreadMessages

        setOnClickListener {
            onItemClickListener.onChannelClick(model)
        }
    }
}

class HomeDirectItemViewHolder(itemView: View) : HomeChannelViewHolder(itemView) {
    override fun bind(
        model: HomeChannelModel,
        onItemClickListener: HomeChannelAdapter.OnItemClickListener
    ) = with(itemView) {
        model as HomeChannelModel.DirectItem

        textView_direct_title.text = model.title

        imageView_direct_unreadIndicator.isVisible = model.hasUnreadMessages

        setOnClickListener {
            onItemClickListener.onDirectClick(model)
        }
    }
}