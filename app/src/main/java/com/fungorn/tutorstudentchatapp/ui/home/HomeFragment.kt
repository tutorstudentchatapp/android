package com.fungorn.tutorstudentchatapp.ui.home

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.ui.chat.ChatActivity
import com.fungorn.tutorstudentchatapp.ui.chat.model.ChatType
import com.fungorn.tutorstudentchatapp.ui.home.model.HomeChannelModel
import com.fungorn.tutorstudentchatapp.ui.utils.ext.doOnQuerySubmit
import com.fungorn.tutorstudentchatapp.utils.ext.observeNonNull
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment(R.layout.fragment_home) {
    private val viewModel: HomeViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchView_home_search.apply {
            findViewById<ImageView>(R.id.search_close_btn).setOnClickListener {
                setQuery("", false)
                clearFocus()
            }
            doOnQuerySubmit { query ->
                Toast.makeText(context, query, Toast.LENGTH_SHORT).show()
            }
        }

        val homeChannelAdapter = HomeChannelAdapter(
            object : HomeChannelAdapter.OnItemClickListener {
                override fun onChannelClick(channel: HomeChannelModel.ChannelItem) {
                    // TODO
                    ChatActivity.openChat(requireActivity(), channel.chatId, ChatType.Group)
                }

                override fun onDirectClick(direct: HomeChannelModel.DirectItem) {
                    // TODO
                    ChatActivity.openChat(requireActivity(), direct.chatId, ChatType.Direct)
                }
            }
        )

        recyclerView_home_channels.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = homeChannelAdapter
        }

        imageView_home_profile.setOnClickListener {
            findNavController().navigate(R.id.fragment_profile)
        }

        viewModel.channels.observeNonNull(viewLifecycleOwner) { channels ->
            homeChannelAdapter.updateItems(channels)
        }
    }
}