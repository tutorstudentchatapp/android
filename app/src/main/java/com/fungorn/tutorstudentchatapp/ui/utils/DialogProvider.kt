package com.fungorn.tutorstudentchatapp.ui.utils

import android.app.AlertDialog
import android.content.Context
import com.fungorn.tutorstudentchatapp.R

object DialogProvider {

    fun confirmationDialog(
        context: Context,
        message: String = context.getString(R.string.common_confirmation),
        cancellable: Boolean = true,
        positiveText: String = context.getString(R.string.common_yes),
        negativeText: String = context.getString(R.string.common_no),
        onConfirm: (() -> Unit),
        onDecline: (() -> Unit)
    ) = AlertDialog.Builder(context).apply {
        setMessage(message)
        setCancelable(cancellable)
        setPositiveButton(positiveText) { dialog, _ ->
            onConfirm()
            dialog.dismiss()
        }
        setNegativeButton(negativeText) { dialog, _ ->
            onDecline()
            dialog.dismiss()
        }
    }.create()

}