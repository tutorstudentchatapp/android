package com.fungorn.tutorstudentchatapp.ui.chat

import android.view.View
import android.widget.RadioButton
import com.fungorn.tutorstudentchatapp.ui.chat.model.ChatMessage
import com.stfalcon.chatkit.messages.MessageHolders
import kotlinx.android.synthetic.main.item_chat_outcoming_text.view.*
import java.text.SimpleDateFormat
import java.util.*


class ChatMessageViewHolder(
    itemView: View,
    payload: Any?
) : MessageHolders.BaseMessageViewHolder<ChatMessage>(itemView, payload) {
    private val listener = payload as ChatFragment.OnAnswerSelectListener

    override fun onBind(message: ChatMessage) = with(itemView) {
        senderNameTextView.text = message.user.name
        messageTextView.text = message.text
        timeTextView.text = SimpleDateFormat("HH:mm", Locale.ENGLISH).format(message.createdAt)

        if (message.poll != null) {
            pollGroup.apply {
                message.poll.options.forEach {
                    addView(
                        RadioButton(context).apply {
                            id = it.id
                            text = it.text
                        }
                    )
                }
                setOnCheckedChangeListener { _, checkedId ->
                    val answer = message.poll.options.find { it.id == checkedId }!!

                    listener.onSelect(answer.id, answer.text)
                }
            }
        }

    }
}