package com.fungorn.tutorstudentchatapp.ui.home.model

sealed class HomeChannelModel(
    open val title: String
) {
    data class HeaderItem(override val title: String) :
        HomeChannelModel(title)

    data class ChannelItem(
        val chatId: Int,
        override val title: String,
        val hasUnreadMessages: Boolean
    ) : HomeChannelModel(title)

    data class DirectItem(
        val chatId: Int,
        override val title: String,
        val hasUnreadMessages: Boolean
    ) :
        HomeChannelModel(title)
}