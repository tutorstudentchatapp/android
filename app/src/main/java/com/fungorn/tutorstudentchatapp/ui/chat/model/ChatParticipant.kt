package com.fungorn.tutorstudentchatapp.ui.chat.model

import com.fungorn.tutorstudentchatapp.domain.AvatarModel
import com.stfalcon.chatkit.commons.models.IUser

data class ChatParticipant(
    val id: Int,
    private val name: String,
    val avatar: AvatarModel
) : IUser {
    override fun getId(): String = id.toString()
    override fun getName(): String = name
    override fun getAvatar(): String = avatar.imageUrl

}