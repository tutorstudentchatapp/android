package com.fungorn.tutorstudentchatapp.domain

enum class UserType {
    TUTOR, STUDENT
}