package com.fungorn.tutorstudentchatapp.domain

enum class MessageType {
    Ordinary,
    Poll
}