package com.fungorn.tutorstudentchatapp.domain

data class AvatarModel(
    val imageUrl: String,
    val color: String
)