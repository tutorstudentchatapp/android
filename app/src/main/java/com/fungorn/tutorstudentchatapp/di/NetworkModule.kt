package com.fungorn.tutorstudentchatapp.di

import com.fungorn.tutorstudentchatapp.BuildConfig
import com.fungorn.tutorstudentchatapp.data.api.AccessTokenAuthenticator
import com.fungorn.tutorstudentchatapp.data.api.DateTimeDeserializer
import com.fungorn.tutorstudentchatapp.data.api.DateTimeSerializer
import com.fungorn.tutorstudentchatapp.data.api.websocket.FlowStreamAdapter
import com.fungorn.tutorstudentchatapp.data.api.websocket.WebsocketChatService
import com.fungorn.tutorstudentchatapp.utils.Const
import com.google.gson.GsonBuilder
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.lifecycle.android.AndroidLifecycle
import com.tinder.scarlet.messageadapter.gson.GsonMessageAdapter
import com.tinder.scarlet.retry.LinearBackoffStrategy
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.joda.time.DateTime
import org.koin.android.ext.koin.androidApplication
import org.koin.core.scope.Scope
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

// https://dev.to/rtficial/kotlin-coroutines-and-retrofit-a-practical-approach-to-consuming-rest-apis-in-android-446k
val networkModule = module {
    single { provideGsonBuilder() }
    factory { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    single { provideScarlet(get()) }

    single { provideWebsocketChatService(get()) }
}

private fun provideGsonBuilder() = GsonBuilder()
    .setDateFormat(Const.DATE_FORMAT)
    .registerTypeAdapter(DateTime::class.java, DateTimeSerializer())
    .registerTypeAdapter(DateTime::class.java, DateTimeDeserializer())

private fun Scope.provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
    Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create((get() as GsonBuilder).create()))
        .build()

private fun Scope.provideScarlet(okHttpClient: OkHttpClient): Scarlet =
    Scarlet.Builder()
        .webSocketFactory(okHttpClient.newWebSocketFactory("wss://echo.websocket.org")) // TODO
        .backoffStrategy(LinearBackoffStrategy(10_000))
        .addMessageAdapterFactory(GsonMessageAdapter.Factory((get() as GsonBuilder).create()))
        .addStreamAdapterFactory(FlowStreamAdapter.Factory)
        .lifecycle(AndroidLifecycle.ofApplicationForeground(androidApplication()))
        .build()

private fun Scope.provideOkHttpClient(): OkHttpClient =
    OkHttpClient().newBuilder()
        .cache(Cache(androidApplication().cacheDir, 10 * 1024 * 1024)) // 10 MB
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .authenticator(AccessTokenAuthenticator(get()))
        .callTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(0, TimeUnit.SECONDS)
        .readTimeout(0, TimeUnit.SECONDS)
        .connectTimeout(0, TimeUnit.SECONDS)
        .build()

private fun provideWebsocketChatService(scarlet: Scarlet) = scarlet.create<WebsocketChatService>()